angular.module('app.controllers', [])
  
.controller('homeCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$http, $interval,$window) 
{

  $scope.pos={}; //POSICAO DO TECNICO

  function callConnect()
  {
    $scope.endereco = {}; //ENDERECO CLIENTE

    $scope.endereco.latitude = -18.915725;
    $scope.endereco.longitude = -48.2550379;
    
    connect();  
    openMap();
  };





  var stompClient = null;

  function connect() 
  {
    var socket = new SockJS('http://localhost:8080/websocket');
    stompClient = Stomp.over(socket);
    
    stompClient.connect({}, function (frame) 
    {
        console.log('Connected: ' + frame);
      
        var subscription = stompClient.subscribe('/topic/greetings', function (greeting) 
        {
           /*console.log(greeting.body);*/

          $scope.pos = JSON.parse(greeting.body);          
          

        },{ id: "mysubid-1" });

        console.log(subscription);
    });
    
  }

  function openMap(enderecoCliente)
  {
/*    console.log("enderecos")
    console.log(enderecoCliente);
    console.log($scope.pos);
*/
   /* window.open("https://www.google.com/maps/dir/"+$scope.pos.latitude+","+$scope.pos.longitude+"/"+
      enderecoCliente.latitude+","+enderecoCliente.longitude);*/
  }


  function disconnect() {
    if (stompClient !== null) {
      stompClient.disconnect();

    }
    setConnected(false);
    stompClient = null;
    console.log("Disconnected");
    
  }

    $(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#verTecnicoButton" ).click(function() { callConnect(); });
    /*$( "#disconnect" ).click(function() { disconnect(); });*/
});



}])
.controller('oSCtrl', 
function ($scope, $stateParams,$http, $interval,$window) 
{

})
.config(function ($httpProvider) {
  $httpProvider.defaults.headers.common = {};
  $httpProvider.defaults.headers.post = {};
  $httpProvider.defaults.headers.put = {};
  $httpProvider.defaults.headers.patch = {};
});
 